package pl.igor.maculewicz.zpsbgallerybackend.utils.crypto.exception;

/**
 * Should be thrown when request verification fails.
 */
public class RequestVerificationException extends Exception {

    public RequestVerificationException(String message) {
        super(message);
    }

    public RequestVerificationException(String message, Throwable cause) {
        super(message, cause);
    }
}
