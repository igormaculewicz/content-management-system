package pl.igor.maculewicz.zpsbgallerybackend.utils.helper;

import lombok.experimental.UtilityClass;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Set;
import java.util.stream.Collectors;

@UtilityClass
public class RoleHelper {

    public static Set<GrantedAuthority> grantRoles(Set<String> roles) {
        return roles.stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toSet());
    }
}
