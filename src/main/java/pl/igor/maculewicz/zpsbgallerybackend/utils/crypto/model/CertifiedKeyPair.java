package pl.igor.maculewicz.zpsbgallerybackend.utils.crypto.model;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.security.PrivateKey;
import java.security.cert.Certificate;

/**
 * Container for pair of certificate and private key
 */
@Getter
@RequiredArgsConstructor
public class CertifiedKeyPair {

    @NonNull
    private final Certificate certificate;

    @NonNull
    private final PrivateKey privateKey;
}
