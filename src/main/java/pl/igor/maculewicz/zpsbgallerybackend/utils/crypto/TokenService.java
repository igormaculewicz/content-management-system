package pl.igor.maculewicz.zpsbgallerybackend.utils.crypto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.DefaultClaims;
import io.jsonwebtoken.impl.DefaultClock;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.util.StringUtils;
import pl.igor.maculewicz.zpsbgallerybackend.utils.crypto.exception.RequestVerificationException;
import pl.igor.maculewicz.zpsbgallerybackend.utils.crypto.model.CertifiedKeyPair;

import javax.validation.constraints.Positive;
import java.lang.reflect.Array;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Manager which realizes a functionality of signing and verifying jwt signed request, and also utilities for reading it.
 */
@RequiredArgsConstructor
public class TokenService {

    private static final Clock CLOCK = DefaultClock.INSTANCE;
    private static final String AUTHORITIES_CLAIM = "authorities";

    @NonNull
    private final CertifiedKeyPair keyPair;

    @NonNull
    private final SignatureAlgorithm signatureAlgorithm;

    @NonNull
    private final Claims defaultClaims;

    @NonNull
    private final ObjectMapper objectMapper;

    @Positive
    private final long tokenDuration;

    public String get(@NonNull String username, Collection<? extends GrantedAuthority> roles) {

        JwtBuilder builder = Jwts
                .builder()
                .setClaims(getDefaultClaims())
                .claim(AUTHORITIES_CLAIM, roles.stream().map(GrantedAuthority::getAuthority).toArray())
                .setAudience(username)
                .signWith(signatureAlgorithm, keyPair.getPrivateKey());

        return builder.compact();
    }

    public UsernamePasswordAuthenticationToken verify(@NonNull String jwtToken) throws RequestVerificationException {
        JwtParser parser = Jwts
                .parser()
                .setSigningKey(keyPair.getCertificate().getPublicKey());

        try {
            Jws<Claims> claimsJws = parser.parseClaimsJws(jwtToken);
            Claims claims = claimsJws.getBody();

            List<String> authorityClaims = claims.get(AUTHORITIES_CLAIM, ArrayList.class);

            Set<GrantedAuthority> authorities = authorityClaims.stream()
                    .map(String::trim)
                    .map(SimpleGrantedAuthority::new)
                    .collect(Collectors.toSet());

            String username = claims.getAudience();

            //Null credentials to prevent memory leak or buffer overflow attacks.
            return new UsernamePasswordAuthenticationToken(username, null, authorities);

        } catch (MalformedJwtException | SignatureException | IllegalArgumentException e) {
            throw new RequestVerificationException("Cannot verify a given signature!", e);
        } catch (ExpiredJwtException e) {
            throw new RequestVerificationException("Given signed request is already expired!", e);
        }
    }

    private Claims getDefaultClaims() {

        Claims claims = new DefaultClaims(new HashMap<>(defaultClaims));

        if (Objects.isNull(defaultClaims.getIssuedAt())) {
            claims.setIssuedAt(CLOCK.now());
        }

        if (StringUtils.isEmpty(defaultClaims.getId())) {
            claims.setId(UUID.randomUUID().toString());
        }

        if (Objects.isNull(defaultClaims.getExpiration())) {
            LocalDateTime plus = LocalDateTime.now().plus(tokenDuration, ChronoUnit.MILLIS);
            Date from = Date.from(plus.atZone(ZoneId.systemDefault()).toInstant());
            claims.setExpiration(from);
        }

        return claims;
    }
}
