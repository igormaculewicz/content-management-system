package pl.igor.maculewicz.zpsbgallerybackend.utils.crypto.exception;

/**
 * Should be thrown when Request signing fails.
 */
public class RequestSigningException extends Exception {
    public RequestSigningException(String message, Throwable cause) {
        super(message, cause);
    }
}
