package pl.igor.maculewicz.zpsbgallerybackend.utils.crypto;

import lombok.Getter;
import lombok.NonNull;
import org.springframework.lang.Nullable;
import pl.igor.maculewicz.zpsbgallerybackend.utils.crypto.model.CertifiedKeyPair;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.Objects;

/**
 * Reader for Java Keystore.
 */
@Getter
public class JKSReader {

    private final KeyStore keystore;

    /**
     * Default constructor
     *
     * @param keystoreFile file keystore
     * @param password     password too keystore. Null if not present.
     * @throws KeyStoreException
     * @throws FileNotFoundException
     */
    public JKSReader(@NonNull File keystoreFile, @Nullable String password) throws KeyStoreException, FileNotFoundException {

        if (!keystoreFile.exists()) {
            throw new FileNotFoundException("Cannot find a keystore!");
        }

        try (FileInputStream keystoreInputStream = new FileInputStream(keystoreFile)) {
            this.keystore = KeyStore.getInstance("JKS");
            getKeystore().load(keystoreInputStream, Objects.isNull(password) ? null : password.toCharArray());
        } catch (CertificateException | NoSuchAlgorithmException | IOException e) {
            throw new KeyStoreException("Cannot build keystore!", e);
        }
    }

    /**
     * Returns a keypair from keystore. Throws exception if keyPair is not present.
     *
     * @param alias    alias of keyPair.
     * @param password password to private key of keyPair.
     * @return If exists, return a keyPair from keystore.
     * @throws KeyStoreException throws when keyPair is not present.
     */
    public CertifiedKeyPair getKeyPair(@NonNull String alias, @Nullable String password) throws KeyStoreException {
        try {
            PrivateKey privateKey = (PrivateKey) getKeystore().getKey(alias, Objects.nonNull(password) ? password.toCharArray() : null);
            Certificate certificate = getKeystore().getCertificate(alias);

            if (Objects.isNull(privateKey) || Objects.isNull(certificate)) {
                throw new UnrecoverableEntryException("Cannot find entry for given alias!");
            }

            return new CertifiedKeyPair(certificate, privateKey);
        } catch (NoSuchAlgorithmException | UnrecoverableEntryException e) {
            throw new KeyStoreException("Cannot obtain keyPair!", e);
        }
    }
}
