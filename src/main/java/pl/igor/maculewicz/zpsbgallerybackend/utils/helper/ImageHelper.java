package pl.igor.maculewicz.zpsbgallerybackend.utils.helper;

import lombok.experimental.UtilityClass;
import org.imgscalr.Scalr;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

@UtilityClass
public class ImageHelper {

    public static byte[] scaleBytesImage(byte[] bytes, int size) throws IOException {
        BufferedImage image = createImageFromBytes(bytes);

        return createBytesFromImage(scale(image, size));
    }

    public static BufferedImage createImageFromBytes(byte[] imageData) throws IOException {
        ByteArrayInputStream bais = new ByteArrayInputStream(imageData);
        return ImageIO.read(bais);
    }

    public static byte[] createBytesFromImage(BufferedImage image) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        boolean foundWriter = ImageIO.write(image, "jpg", baos);
        return baos.toByteArray();
    }

    public static BufferedImage scale(BufferedImage source, int size) {
      return Scalr.resize(source, Scalr.Method.AUTOMATIC, Scalr.Mode.AUTOMATIC, size, Scalr.OP_ANTIALIAS);
    }

}
