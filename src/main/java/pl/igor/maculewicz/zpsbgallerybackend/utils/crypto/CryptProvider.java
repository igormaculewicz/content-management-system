package pl.igor.maculewicz.zpsbgallerybackend.utils.crypto;

import lombok.experimental.UtilityClass;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@UtilityClass
public class CryptProvider {

    private final static BCryptPasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();

    public static PasswordEncoder getProvider() {
        return PASSWORD_ENCODER;
    }
}
