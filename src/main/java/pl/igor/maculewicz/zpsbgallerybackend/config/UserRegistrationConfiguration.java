package pl.igor.maculewicz.zpsbgallerybackend.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties("user.registration")
public class UserRegistrationConfiguration {
    private String usernameRegexp;
    private String wrongUsernameMessage;

    private String passwordRegexp;
    private String wrongPasswordMessage;
}

