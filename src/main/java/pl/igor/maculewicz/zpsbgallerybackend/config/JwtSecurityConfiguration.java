package pl.igor.maculewicz.zpsbgallerybackend.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.io.File;

@Data
@Configuration
@ConfigurationProperties("jwt-security.secret-key")
public class JwtSecurityConfiguration {
    private File file;
    private String password;
}

