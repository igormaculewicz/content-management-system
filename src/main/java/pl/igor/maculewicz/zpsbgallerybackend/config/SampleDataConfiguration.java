package pl.igor.maculewicz.zpsbgallerybackend.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties("generators.sample-data")
public class SampleDataConfiguration {
    private boolean adminUser;
    private boolean news;
    private boolean galleries;
}

