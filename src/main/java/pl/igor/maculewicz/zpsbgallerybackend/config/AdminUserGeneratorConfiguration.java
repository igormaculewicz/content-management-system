package pl.igor.maculewicz.zpsbgallerybackend.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties("generator.admin-user")
public class AdminUserGeneratorConfiguration {
    private String username;
    private String password;
}

