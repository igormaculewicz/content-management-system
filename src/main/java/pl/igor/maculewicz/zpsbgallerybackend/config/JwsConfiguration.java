package pl.igor.maculewicz.zpsbgallerybackend.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.DefaultClaims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.igor.maculewicz.zpsbgallerybackend.utils.crypto.JKSReader;
import pl.igor.maculewicz.zpsbgallerybackend.utils.crypto.TokenService;
import pl.igor.maculewicz.zpsbgallerybackend.utils.crypto.model.CertifiedKeyPair;

import java.io.File;
import java.io.IOException;
import java.security.KeyStoreException;


//TODO change @Value to @ConfigurationProperties
@Configuration
public class JwsConfiguration {

    @Value("${jws.keystore.path}")
    private File keystoreFile;

    @Value("${jws.keystore.password:#{null}}")
    private String keystorePassword;

    @Value("${jws.keypair.alias}")
    private String keypairAlias;

    @Value("${jws.keypair.password:#{null}}")
    private String keypairPassword;

    @Value("${jws.signature.algorithm:RS512}")
    private String signatureAlgorithm;

    @Value("${jws.signature.issuer:zpsb-gallery-backend}")
    private String signatureIssuer;

    @Value("${jwt.signature.duration-time-in-millis:180000}")
    private long jwsDuration;

    @Bean
    public CertifiedKeyPair jwsSigningKeypair() throws IOException, KeyStoreException {
        JKSReader reader = new JKSReader(keystoreFile, keystorePassword);

        return reader.getKeyPair(keypairAlias, keypairPassword);
    }

    @Bean
    @Autowired
    public TokenService jwtManager(CertifiedKeyPair keypair, ObjectMapper objectMapper) {
        return new TokenService(keypair, SignatureAlgorithm.forName(signatureAlgorithm), getDefaultClaims(), objectMapper, jwsDuration);
    }

    private Claims getDefaultClaims() {
        Claims claims = new DefaultClaims();

        claims.setIssuer(signatureIssuer);

        return claims;
    }
}
