package pl.igor.maculewicz.zpsbgallerybackend.domain.manager;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.Nullable;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.NewsEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.UserEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.repository.UserRepository;
import pl.igor.maculewicz.zpsbgallerybackend.domain.exception.ForbiddenException;
import pl.igor.maculewicz.zpsbgallerybackend.domain.exception.NotFoundException;
import pl.igor.maculewicz.zpsbgallerybackend.domain.exception.UnauthorisedException;
import pl.igor.maculewicz.zpsbgallerybackend.domain.model.SimpleNews;
import pl.igor.maculewicz.zpsbgallerybackend.domain.service.NewsService;

@Component
@RequiredArgsConstructor
public class NewsManager {

    private final NewsService newsService;
    private final ObjectMapper objectMapper;
    private final UserRepository userRepository;
    private final LoggingManager loggingManager;

    public Page<SimpleNews> getVisibleByPageRequest(@NonNull Pageable pageable) {
        loggingManager.info("Trying to get all visible news");
        Page<NewsEntity> page = newsService.getAllVisible(pageable);

        return page.map(el -> objectMapper.convertValue(el, SimpleNews.class));
    }

    public NewsEntity getVisibleById(long id) throws NotFoundException, ForbiddenException, UnauthorisedException {
        loggingManager.info("Trying to get visible news by id: {}", id);

        NewsEntity news = newsService.get(id);

        if (!news.visible() && !UserManager.isAdminOrGivenUser(news.creator().username())) {
            throw new ForbiddenException();
        }

        return news;
    }

    public Page<SimpleNews> getVisibleByUsername(String username, Pageable pageable) {
        UserEntity user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(String.format("User '%s' not found!", username)));

        return newsService.getAllVisibleByCreator(user, pageable).map(el -> objectMapper.convertValue(el, SimpleNews.class));
    }

    public Page<SimpleNews> getAllByUsername(String username, Pageable pageable) throws UnauthorisedException {

        UserEntity user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(String.format("User '%s' not found!", username)));

        return newsService.getAllByCreator(user, pageable).map(el -> objectMapper.convertValue(el, SimpleNews.class));
    }


    public long create(String title, String content, boolean visible) throws UnauthorisedException {
        loggingManager.info("Trying to create news with title: {}", title);

        UsernamePasswordAuthenticationToken loggedUser = UserManager.getLoggedUser();

        UserEntity user = userRepository.findByUsername(loggedUser.getName())
                .orElseThrow(() -> new UsernameNotFoundException(String.format("User '%s' not found!", loggedUser.getName())));

        return newsService.create(title, content, visible, user).id();
    }

    public NewsEntity update(long id, @Nullable String title, @Nullable String content, @Nullable Boolean visible) throws NotFoundException, UnauthorisedException, ForbiddenException {
        loggingManager.info("Trying to update news with id: {}", id);

        NewsEntity news = newsService.get(id);

        if (!UserManager.isAdminOrGivenUser(news.creator().username())) {
            throw new ForbiddenException();
        }

        return newsService.update(news, title, content, visible);
    }

    public void delete(long newsId) throws NotFoundException, ForbiddenException, UnauthorisedException {
        loggingManager.info("Trying to delete gallery with id: {}", newsId);

        NewsEntity news = newsService.get(newsId);

        if (!UserManager.isAdminOrGivenUser(news.creator().username())) {
            throw new ForbiddenException();
        }

        newsService.delete(news);
    }
}
