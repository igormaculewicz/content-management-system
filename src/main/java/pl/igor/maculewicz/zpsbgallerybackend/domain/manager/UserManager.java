package pl.igor.maculewicz.zpsbgallerybackend.domain.manager;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import pl.igor.maculewicz.zpsbgallerybackend.config.UserRegistrationConfiguration;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.UserEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.dictionary.Role;
import pl.igor.maculewicz.zpsbgallerybackend.domain.exception.*;
import pl.igor.maculewicz.zpsbgallerybackend.domain.service.UserService;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class UserManager {

    private final UserRegistrationConfiguration userRegistrationConfiguration;
    private final UserService userService;
    private final LoggingManager loggingManager;

    public final static String ANONYMOUS_USERNAME = "anonymous";

    public static boolean isAdmin() throws UnauthorisedException {
        return getLoggedUser()
                .getAuthorities()
                .stream()
                .anyMatch(grantedAuthority -> grantedAuthority.getAuthority().toUpperCase().equals(Role.ROLE_ADMIN.name()));
    }

    public static boolean isGivenUser(String username) throws UnauthorisedException {
        return getLoggedUser().getPrincipal().equals(username);
    }

    public static boolean isAdminOrGivenUser(String username) throws UnauthorisedException {
        return isAdmin() || isGivenUser(username);
    }

    public static UsernamePasswordAuthenticationToken getLoggedUser() throws UnauthorisedException {
        return (UsernamePasswordAuthenticationToken) Optional.of(SecurityContextHolder.getContext())
                .map(SecurityContext::getAuthentication)
                .filter(user -> !user.getPrincipal().equals(ANONYMOUS_USERNAME))
                .orElseThrow(() -> new UnauthorisedException("You are not authorised. Please log in!"));
    }

    public UserEntity getCurrentUser() throws UnauthorisedException, NotFoundException {
        loggingManager.info("Trying to get current user");

        return userService.get(UserManager.getLoggedUser().getName());
    }

    public UserEntity get(long id) throws NotFoundException, ForbiddenException, UnauthorisedException {
        loggingManager.info("Trying to get user with id: {}", id);

        UserEntity user = userService.get(id);

        if (!UserManager.isAdminOrGivenUser(user.username())) {
            throw new ForbiddenException();
        }

        return user;
    }

    public UserEntity get(String username) throws NotFoundException {
        loggingManager.info("Trying to get user with username: {}", username);

        return userService.get(username);
    }

    public UserEntity createUser(String username, String password) throws BadRequestException, AlreadyExistsException {
        return create(username, password, Role.ROLE_USER);
    }

    public UserEntity createAdmin(String username, String password) throws BadRequestException, UnauthorisedException, ForbiddenException, AlreadyExistsException {

        if (!UserManager.isAdmin()) {
            throw new ForbiddenException();
        }

        return create(username, password, Role.ROLE_ADMIN);
    }

    public UserEntity changePassword(@NonNull String password) throws BadRequestException, NotFoundException, UnauthorisedException {
        loggingManager.info("Trying to change password");

        if (!password.matches(userRegistrationConfiguration.getPasswordRegexp())) {
            throw new BadRequestException(userRegistrationConfiguration.getWrongPasswordMessage());
        }

        return userService.update(UserManager.getLoggedUser().getName(), password);
    }


    public void delete(String username) throws ForbiddenException, UnauthorisedException, NotFoundException {
        loggingManager.info("Trying to delete account of user: {}", username);

        if (!UserManager.isAdminOrGivenUser(username)) {
            throw new ForbiddenException();
        }

        userService.delete(username);
    }

    private UserEntity create(String username, String password, Role... roles) throws BadRequestException, AlreadyExistsException {
        loggingManager.info("Trying to create user with username: {}, and roles: {}", username, roles);

        if (!username.matches(userRegistrationConfiguration.getUsernameRegexp())) {
            throw new BadRequestException(userRegistrationConfiguration.getWrongUsernameMessage());
        }

        if (!password.matches(userRegistrationConfiguration.getPasswordRegexp())) {
            throw new BadRequestException(userRegistrationConfiguration.getWrongPasswordMessage());
        }

        return userService.create(username, password, roles);
    }
}
