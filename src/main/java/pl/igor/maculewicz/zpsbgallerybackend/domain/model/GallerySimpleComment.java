package pl.igor.maculewicz.zpsbgallerybackend.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.UserEntity;

import java.time.LocalDateTime;

@Getter
@Accessors(fluent = true)
@RequiredArgsConstructor
public class GallerySimpleComment {
    private final long id;
    private final String title;
    private final String comment;

    @JsonIgnoreProperties("roles")
    private final UserEntity creator;
    private final LocalDateTime createdAt;
}
