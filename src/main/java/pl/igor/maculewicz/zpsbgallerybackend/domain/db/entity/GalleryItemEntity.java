package pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CreationTimestamp;
import pl.igor.maculewicz.zpsbgallerybackend.domain.model.GallerySimpleItem;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@Accessors(fluent = true)
@Entity(name = "gallery_item")
public class GalleryItemEntity {

    @Id
    @SequenceGenerator(name = "galleryItemSeqGen", sequenceName = "gallery_item_id_seq", initialValue = 5, allocationSize = 100)
    @GeneratedValue(generator = "galleryItemSeqGen")
    private Long id;

    @ManyToOne
    private GalleryEntity gallery;

    @Column(length = 256)
    private String name;

    @Column(length = 1024)
    private String description;

    @Column(length = 64, nullable = false, unique = true)
    private String filename;

    @CreationTimestamp
    @Column(updatable = false, nullable = false)
    private LocalDateTime createdAt;

    @JsonValue
    public GallerySimpleItem toGallerySimpleItem() {
        return new GallerySimpleItem(id, name, description, filename, createdAt);
    }
}
























