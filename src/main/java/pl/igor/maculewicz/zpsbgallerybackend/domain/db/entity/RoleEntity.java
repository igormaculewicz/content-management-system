package pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity;

import lombok.*;
import lombok.experimental.Accessors;
import pl.igor.maculewicz.zpsbgallerybackend.domain.dictionary.Role;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity(name = "role")
@Accessors(fluent = true)
public class RoleEntity {

    @Id
    @SequenceGenerator(name = "roleSeqGen", sequenceName = "role_id_seq", initialValue = 5, allocationSize = 100)
    @GeneratedValue(generator = "roleSeqGen")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(unique = true, nullable = false)
    private Role name;
}
