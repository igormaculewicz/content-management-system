package pl.igor.maculewicz.zpsbgallerybackend.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import pl.igor.maculewicz.zpsbgallerybackend.domain.converter.serializer.ContentToDescriptionConverter;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.UserEntity;

import java.time.LocalDateTime;

@Getter
@Accessors(fluent = true)
@RequiredArgsConstructor
public class SimpleNews {

    private final Long id;

    private final String title;

    @JsonDeserialize(converter = ContentToDescriptionConverter.class)
    private final String content;

    private final boolean visible;

    @JsonIgnoreProperties("roles")
    private final UserEntity creator;

    private final LocalDateTime createdAt;

}
