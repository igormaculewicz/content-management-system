package pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.JoinFormula;
import pl.igor.maculewicz.zpsbgallerybackend.domain.model.SimpleGallery;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;

@Data
@NoArgsConstructor
@Entity(name = "gallery")
@Accessors(fluent = true)
@EqualsAndHashCode(exclude = {"galleryItems", "galleryComments"})
@ToString(exclude = {"galleryItems", "galleryComments"})
@JsonIgnoreProperties({"galleryItems", "galleryComments"})
public class GalleryEntity {

    @Id
    @SequenceGenerator(name = "gallerySeqGen", sequenceName = "gallery_id_seq", initialValue = 5, allocationSize = 100)
    @GeneratedValue(generator = "gallerySeqGen")
    private Long id;

    @Column(length = 256)
    private String name;

    @Column(length = 1024)
    private String description;

    @ManyToOne
    private UserEntity creator;

    @Column(nullable = false)
    private boolean privateGallery;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "gallery")
    private Set<GalleryItemEntity> galleryItems;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "gallery")
    private Set<GalleryCommentEntity> galleryComments;

    @ManyToOne
    @JoinFormula("(SELECT gi.id FROM gallery_item gi WHERE gi.gallery_id = id ORDER BY gi.created_at DESC LIMIT 1)")
    private GalleryItemEntity thumbnailImage;

    @CreationTimestamp
    @Column(updatable = false, nullable = false)
    private LocalDateTime createdAt;

    @JsonValue
    public SimpleGallery toSimpleGallery() {
        return new SimpleGallery(id, name, description, Optional.ofNullable(thumbnailImage).map(GalleryItemEntity::filename).orElse(null), creator, privateGallery, createdAt);
    }
}
























