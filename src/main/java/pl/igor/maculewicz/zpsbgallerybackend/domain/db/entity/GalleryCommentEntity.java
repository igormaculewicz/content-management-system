package pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CreationTimestamp;
import pl.igor.maculewicz.zpsbgallerybackend.domain.model.GallerySimpleComment;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@Entity(name = "gallery_comment")
@Accessors(fluent = true)
public class GalleryCommentEntity {

    @Id
    @SequenceGenerator(name = "galleryCommentSeqGen", sequenceName = "gallery_comment_id_seq", initialValue = 5, allocationSize = 100)
    @GeneratedValue(generator = "galleryCommentSeqGen")
    private Long id;

    @ManyToOne
    private GalleryEntity gallery;

    @Column(length = 256)
    private String title;

    @Column(length = 1024, nullable = false)
    private String comment;

    @ManyToOne
    private UserEntity creator;

    @CreationTimestamp
    @Column(updatable = false, nullable = false)
    private LocalDateTime createdAt;

    @JsonValue
    public GallerySimpleComment toGallerySimpleComment() {
        return new GallerySimpleComment(id, title, comment, creator, createdAt);
    }
}
























