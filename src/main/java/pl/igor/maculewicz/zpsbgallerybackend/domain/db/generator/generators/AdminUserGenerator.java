package pl.igor.maculewicz.zpsbgallerybackend.domain.db.generator.generators;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.igor.maculewicz.zpsbgallerybackend.config.AdminUserGeneratorConfiguration;
import pl.igor.maculewicz.zpsbgallerybackend.config.SampleDataConfiguration;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.generator.DataGenerator;
import pl.igor.maculewicz.zpsbgallerybackend.domain.dictionary.Role;
import pl.igor.maculewicz.zpsbgallerybackend.domain.exception.AlreadyExistsException;
import pl.igor.maculewicz.zpsbgallerybackend.domain.service.UserService;

@Component
@RequiredArgsConstructor
public class AdminUserGenerator implements DataGenerator {

    private final SampleDataConfiguration sampleDataConfiguration;
    private final AdminUserGeneratorConfiguration adminUserGeneratorConfiguration;
    private final UserService userService;

    @Override
    public void generate() throws AlreadyExistsException {

        if (!sampleDataConfiguration.isAdminUser()) {
            return;
        }

        userService.create(
                adminUserGeneratorConfiguration.getUsername(),
                adminUserGeneratorConfiguration.getPassword(),
                Role.ROLE_USER, Role.ROLE_ADMIN
        );
    }
}
