package pl.igor.maculewicz.zpsbgallerybackend.domain.service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.GalleryEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.GalleryItemEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.repository.GalleryItemRepository;
import pl.igor.maculewicz.zpsbgallerybackend.domain.exception.NotFoundException;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public class GalleryItemService {

    private final GalleryItemRepository galleryItemRepository;

    public GalleryItemEntity get(String galleryItemName) throws NotFoundException {
        return galleryItemRepository.getFirstByFilename(galleryItemName)
                .orElseThrow(() -> new NotFoundException(String.format("Cannot find gallery item with name: %s", galleryItemName)));
    }

    public GalleryItemEntity get(long galleryItemId) throws NotFoundException {
        return galleryItemRepository.findById(galleryItemId)
                .orElseThrow(() -> new NotFoundException(String.format("Cannot find gallery item with id: %d", galleryItemId)));
    }

    public Page<GalleryItemEntity> get(GalleryEntity gallery, @NonNull Pageable pageable) {
        return galleryItemRepository.getAllByGallery(gallery, pageable);
    }

    public GalleryItemEntity create(@NonNull GalleryEntity gallery, @Nullable String name, @Nullable String description, @NonNull String filename) {

        GalleryItemEntity entity = new GalleryItemEntity()
                .gallery(gallery)
                .name(name)
                .description(description)
                .filename(filename);

        return galleryItemRepository.save(entity);
    }

    public GalleryItemEntity update(long galleryItemId, @Nullable String name, @Nullable String description) throws NotFoundException {

        GalleryItemEntity item = get(galleryItemId);

        if (Objects.nonNull(name)) {
            item.name(name);
        }

        if (Objects.nonNull(description)) {
            item.description(description);
        }

        return galleryItemRepository.save(item);
    }

    public void delete(@NonNull GalleryItemEntity galleryItem) {
        galleryItemRepository.delete(galleryItem);
    }
}
