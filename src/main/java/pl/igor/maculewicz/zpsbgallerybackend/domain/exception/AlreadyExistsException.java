package pl.igor.maculewicz.zpsbgallerybackend.domain.exception;

public class AlreadyExistsException extends Exception {
    public AlreadyExistsException(String message) {
        super(message);
    }
}
