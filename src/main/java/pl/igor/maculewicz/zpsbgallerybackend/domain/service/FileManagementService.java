package pl.igor.maculewicz.zpsbgallerybackend.domain.service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.igor.maculewicz.zpsbgallerybackend.config.FileManagementConfiguration;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class FileManagementService {

    private final FileManagementConfiguration configuration;

    public Path saveFileWithRandomName(byte[] content) {
        return saveFile(UUID.randomUUID().toString(), content);
    }

    public Path saveFile(@NonNull String filename, byte[] content) {
        File directory = new File(configuration.getRepositoryPath());

        if (!directory.exists() && !directory.mkdirs()) {
            throw new IllegalStateException(String.format("Cannot create directories structures for path: %s", configuration.getRepositoryPath()));
        }

        try {
            return Files.write(Path.of(directory.getAbsolutePath(), filename), content);
        } catch (
                IOException e) {
            throw new IllegalStateException("Cannot save image!", e);
        }
    }

    public byte[] getFile(@NonNull String filename) throws IOException {
        return Files.readAllBytes(Paths.get(configuration.getRepositoryPath(), filename));
    }
}
