package pl.igor.maculewicz.zpsbgallerybackend.domain.db.generator;

import java.io.IOException;

@FunctionalInterface
public interface DataGenerator {
    void generate() throws Exception;
}
