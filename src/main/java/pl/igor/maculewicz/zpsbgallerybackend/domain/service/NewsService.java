package pl.igor.maculewicz.zpsbgallerybackend.domain.service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.NewsEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.UserEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.repository.NewsRepository;
import pl.igor.maculewicz.zpsbgallerybackend.domain.exception.NotFoundException;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public class NewsService {

    private final NewsRepository repository;

    public NewsEntity get(long id) throws NotFoundException {
        return repository.findById(id).orElseThrow(() -> new NotFoundException(String.format("Cannot find news with id: %s", id)));
    }

    public Page<NewsEntity> getAllVisible(@NonNull Pageable pageable) {
        return repository.getNewsEntitiesByVisible(true, pageable);
    }

    public Page<NewsEntity> getAllVisibleByCreator(UserEntity creator, @NonNull Pageable pageable) {
        return repository.getNewsEntitiesByVisibleTrueAndCreator(creator, pageable);
    }

    public Page<NewsEntity> getAllByCreator(UserEntity creator, @NonNull Pageable pageable) {
        return repository.getNewsEntitiesByCreator(creator, pageable);
    }

    public NewsEntity getVisible(long id) throws NotFoundException {
        return repository.getFirstByIdAndVisible(id, true).orElseThrow(
                () -> new NotFoundException(String.format("Cannot find news with id: %s", id))
        );
    }

    public NewsEntity create(@Nullable String title, @NonNull String content, boolean visible, @NonNull UserEntity creator) {
        NewsEntity entity = new NewsEntity()
                .title(title)
                .content(content)
                .visible(visible)
                .creator(creator);

        return repository.save(entity);
    }

    public NewsEntity update(@NonNull NewsEntity news, @Nullable String title, @Nullable String content, @Nullable Boolean visible) {

        if (Objects.nonNull(title)) {
            news.title(title);
        }

        if (Objects.nonNull(content)) {
            news.content(content);
        }

        if (Objects.nonNull(visible)) {
            news.visible(visible);
        }

        return repository.save(news);
    }

    public void delete(@NonNull NewsEntity news) {
        repository.delete(news);
    }
}
