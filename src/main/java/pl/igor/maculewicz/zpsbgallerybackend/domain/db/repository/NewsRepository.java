package pl.igor.maculewicz.zpsbgallerybackend.domain.db.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.NewsEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.UserEntity;

import java.util.Optional;

@Repository
public interface NewsRepository extends JpaRepository<NewsEntity, Long> {

    Page<NewsEntity> getNewsEntitiesByVisible(boolean visible, Pageable pageable);

    Page<NewsEntity> getNewsEntitiesByVisibleTrueAndCreator(UserEntity creator, Pageable pageable);

    Page<NewsEntity> getNewsEntitiesByCreator(UserEntity creator, Pageable pageable);

    Optional<NewsEntity> getFirstByIdAndVisible(long id, boolean visible);

}
