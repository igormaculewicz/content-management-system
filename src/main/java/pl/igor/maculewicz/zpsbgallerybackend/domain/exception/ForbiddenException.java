package pl.igor.maculewicz.zpsbgallerybackend.domain.exception;

public class ForbiddenException extends Exception {

    public ForbiddenException() {
        super("You don't have access to this resource!");
    }
}
