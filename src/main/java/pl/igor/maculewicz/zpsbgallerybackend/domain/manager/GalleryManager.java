package pl.igor.maculewicz.zpsbgallerybackend.domain.manager;


import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.tika.Tika;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.igor.maculewicz.zpsbgallerybackend.config.GalleriesConfiguration;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.GalleryCommentEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.GalleryEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.GalleryItemEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.UserEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.exception.BadRequestException;
import pl.igor.maculewicz.zpsbgallerybackend.domain.exception.ForbiddenException;
import pl.igor.maculewicz.zpsbgallerybackend.domain.exception.NotFoundException;
import pl.igor.maculewicz.zpsbgallerybackend.domain.exception.UnauthorisedException;
import pl.igor.maculewicz.zpsbgallerybackend.domain.service.*;
import pl.igor.maculewicz.zpsbgallerybackend.utils.helper.ImageHelper;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class GalleryManager {

    private final UserService userService;
    private final FileManagementService fileManagementService;
    private final GalleryService galleryService;
    private final GalleryItemService galleryItemService;
    private final GalleryCommentService galleryCommentService;

    private final LoggingManager loggingManager;
    private final GalleriesConfiguration galleriesConfiguration;

    public Page<GalleryEntity> getAllPublic(@NonNull Pageable pageable) {
        loggingManager.info("Getting all public galleries");

        return galleryService.getAllPublic(pageable);
    }

    public Page<GalleryEntity> getPublicByUsername(@NonNull String username, @NonNull Pageable pageable) throws NotFoundException {
        loggingManager.info("Get all public galleries of user: {}", username);

        return galleryService.getPublicByUsername(username, pageable);
    }

    public Page<GalleryEntity> getAllByUsername(@NonNull String username, @NonNull Pageable pageable) throws ForbiddenException, UnauthorisedException {
        loggingManager.info("Get all galleries of user: {}", username);

        if (!UserManager.isAdminOrGivenUser(username)) {
            throw new ForbiddenException();
        }

        return galleryService.getAllByUsername(username, pageable);
    }

    public GalleryEntity get(long galleryId) throws NotFoundException, UnauthorisedException, ForbiddenException {
        loggingManager.info("Trying to obtain gallery with id: {}", galleryId);

        GalleryEntity gallery = galleryService.getById(galleryId);

        if (gallery.privateGallery() && !UserManager.isAdminOrGivenUser(gallery.creator().username())) {
            throw new ForbiddenException();
        }

        return gallery;
    }

    public GalleryEntity create(String name, String description, boolean privateGallery) throws NotFoundException, UnauthorisedException {
        loggingManager.info("Trying to create gallery with name: {}", name);

        UserEntity user = userService.get(UserManager.getLoggedUser().getName());

        return galleryService.create(name, description, privateGallery, user);
    }

    public GalleryEntity update(long galleryId, @Nullable String name, @Nullable String description, @Nullable Boolean privateGallery) throws NotFoundException, ForbiddenException, UnauthorisedException {
        loggingManager.info("Trying to update gallery with id: {}", galleryId);

        GalleryEntity gallery = galleryService.getById(galleryId);

        if (!UserManager.isAdminOrGivenUser(gallery.creator().username())) {
            throw new ForbiddenException();
        }

        return galleryService.update(gallery, name, description, privateGallery);
    }

    public void delete(long galleryId) throws NotFoundException, ForbiddenException, UnauthorisedException {
        loggingManager.info("Trying to delete gallery with id: {}", galleryId);

        GalleryEntity gallery = galleryService.getById(galleryId);

        if (!UserManager.isAdminOrGivenUser(gallery.creator().username())) {
            throw new ForbiddenException();
        }

        galleryService.delete(gallery);
    }

    public byte[] getItem(String imageName) throws NotFoundException, UnauthorisedException, ForbiddenException, IOException {

        GalleryItemEntity galleryItem = galleryItemService.get(imageName);

        if (galleryItem.gallery().privateGallery() && !UserManager.isAdminOrGivenUser(galleryItem.gallery().creator().username())) {
            throw new ForbiddenException();
        }

        return fileManagementService.getFile(imageName);
    }

    public byte[] getScaledItem(String imageName, int size) throws IOException, UnauthorisedException, NotFoundException, ForbiddenException {
        return ImageHelper.scaleBytesImage(getItem(imageName), size);
    }

    public Page<GalleryItemEntity> getItems(long galleryId, @NonNull Pageable pageable) throws NotFoundException, ForbiddenException, UnauthorisedException {
        loggingManager.info("Trying to get items of gallery with id: {}", galleryId);

        GalleryEntity gallery = galleryService.getById(galleryId);

        if (gallery.privateGallery() && !UserManager.isAdminOrGivenUser(gallery.creator().username())) {
            throw new ForbiddenException();
        }

        return galleryItemService.get(gallery, pageable);
    }

    public GalleryItemEntity addItem(GalleryEntity gallery, String name, String description, String base64EncodedFile) throws ForbiddenException, UnauthorisedException, IOException, BadRequestException {
        loggingManager.info("Trying to add item to gallery with id: {}", gallery.id());

        byte[] imageBytes = getBytesFromBase64(base64EncodedFile);

        String mediaType = new Tika().detect(new ByteArrayInputStream(imageBytes));

        if (!galleriesConfiguration.getAllowedMediaTypes().contains(mediaType)) {
            throw new BadRequestException("Unsupported media type!");
        }

        if (!UserManager.isAdminOrGivenUser(gallery.creator().username())) {
            throw new ForbiddenException();
        }

        String filename = fileManagementService.saveFileWithRandomName(imageBytes).getFileName().toString();

        return galleryItemService.create(gallery, name, description, filename);
    }

    public GalleryItemEntity addItem(long galleryId, String name, String description, String base64EncodedFile) throws NotFoundException, ForbiddenException, UnauthorisedException, IOException, BadRequestException {

        GalleryEntity gallery = galleryService.getById(galleryId);

        return addItem(gallery, name, description, base64EncodedFile);
    }


    public GalleryItemEntity updateItem(long galleryItemId, @Nullable String name, @Nullable String description) throws ForbiddenException, UnauthorisedException, NotFoundException {
        loggingManager.info("Trying to delete gallery item with id: {}", galleryItemId);

        GalleryItemEntity item = galleryItemService.get(galleryItemId);

        if (!UserManager.isAdminOrGivenUser(item.gallery().creator().username())) {
            throw new ForbiddenException();
        }

        return galleryItemService.update(galleryItemId, name, description);
    }

    public void deleteItem(long galleryItemId) throws NotFoundException, ForbiddenException, UnauthorisedException {
        loggingManager.info("Trying to delete gallery item with id: {}", galleryItemId);

        GalleryItemEntity item = galleryItemService.get(galleryItemId);

        if (!UserManager.isAdminOrGivenUser(item.gallery().creator().username())) {
            throw new ForbiddenException();
        }

        galleryItemService.delete(item);
    }

    public GalleryCommentEntity getComment(long commentId) throws NotFoundException, UnauthorisedException, ForbiddenException {
        GalleryCommentEntity comment = galleryCommentService.getById(commentId);

        if (comment.gallery().privateGallery() && !UserManager.isAdminOrGivenUser(comment.gallery().creator().username())) {
            throw new ForbiddenException();
        }

        return comment;
    }

    public Page<GalleryCommentEntity> getComments(long galleryId, @NonNull Pageable pageable) throws NotFoundException {
        loggingManager.info("Trying to get comments of gallery with id: {}", galleryId);

        GalleryEntity gallery = galleryService.getById(galleryId);


        return galleryCommentService.get(gallery, pageable);
    }

    public GalleryCommentEntity addComment(long galleryId, @Nullable String title, @NonNull String comment) throws UnauthorisedException, NotFoundException {
        loggingManager.info("Trying to add comment to gallery with id: {}", galleryId);

        GalleryEntity gallery = galleryService.getById(galleryId);


        UserEntity user = userService.get(UserManager.getLoggedUser().getName());

        return galleryCommentService.create(gallery, title, comment, user);
    }

    public void deleteComment(long commentId) throws NotFoundException, UnauthorisedException, ForbiddenException {
        loggingManager.info("Trying to delete comment with id: {}", commentId);

        GalleryCommentEntity comment = galleryCommentService.getById(commentId);

        if (!UserManager.isAdminOrGivenUser(comment.gallery().creator().username())) {
            throw new ForbiddenException();
        }

        galleryCommentService.delete(comment);
    }

    private byte[] getBytesFromBase64(String base64EncodedFile) throws BadRequestException {
        byte[] imageBytes;
        try {
            imageBytes = Base64.getDecoder().decode(base64EncodedFile);
        } catch (IllegalArgumentException e) {
            throw new BadRequestException("Wrong base64 signature!");
        }
        return imageBytes;
    }
}
