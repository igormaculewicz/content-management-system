package pl.igor.maculewicz.zpsbgallerybackend.domain.db.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.GalleryEntity;

@Repository
public interface GalleryRepository extends JpaRepository<GalleryEntity, Long> {

    Page<GalleryEntity> getAllByPrivateGalleryIsFalse(Pageable pageable);

    Page<GalleryEntity> getAllByCreatorUsername(String username, Pageable pageable);

    Page<GalleryEntity> getAllByCreatorUsernameAndPrivateGalleryIsFalse(String username, Pageable pageable);

}
