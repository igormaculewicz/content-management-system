package pl.igor.maculewicz.zpsbgallerybackend.domain.service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.GalleryCommentEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.GalleryEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.UserEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.repository.GalleryCommentRepository;
import pl.igor.maculewicz.zpsbgallerybackend.domain.exception.NotFoundException;

@Service
@RequiredArgsConstructor
public class GalleryCommentService {

    private final GalleryCommentRepository galleryCommentRepository;
    private final UserService userService;

    public GalleryCommentEntity getById(long galleryItemId) throws NotFoundException {
        return galleryCommentRepository.findById(galleryItemId)
                .orElseThrow(() -> new NotFoundException(String.format("Cannot find gallery comment with id: %d", galleryItemId)));
    }

    public Page<GalleryCommentEntity> get(@NonNull GalleryEntity gallery, @NonNull Pageable pageable) {
        return galleryCommentRepository.getAllByGallery(gallery, pageable);
    }

    public GalleryCommentEntity create(@NonNull GalleryEntity gallery, @Nullable String title, @NonNull String comment, @NonNull UserEntity user) {

        GalleryCommentEntity entity = new GalleryCommentEntity()
                .gallery(gallery)
                .title(title)
                .comment(comment)
                .creator(user);

        return galleryCommentRepository.save(entity);
    }

    public void delete(@NonNull GalleryCommentEntity comment) {
        galleryCommentRepository.delete(comment);
    }
}
