package pl.igor.maculewicz.zpsbgallerybackend.domain.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Getter
@Accessors(fluent = true)
@RequiredArgsConstructor
public class GallerySimpleItem {
    private final long id;
    private final String name;
    private final String description;
    private final String path;
    private final LocalDateTime createdAt;
}
