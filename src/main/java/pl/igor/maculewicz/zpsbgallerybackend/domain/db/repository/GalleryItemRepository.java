package pl.igor.maculewicz.zpsbgallerybackend.domain.db.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.GalleryEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.GalleryItemEntity;

import java.util.Optional;

@Repository
public interface GalleryItemRepository extends JpaRepository<GalleryItemEntity, Long> {

    Optional<GalleryItemEntity> getFirstByFilename(String filename);

    Page<GalleryItemEntity> getAllByGallery(GalleryEntity gallery, Pageable pageable);
}
