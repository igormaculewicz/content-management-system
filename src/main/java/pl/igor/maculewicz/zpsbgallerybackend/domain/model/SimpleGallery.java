package pl.igor.maculewicz.zpsbgallerybackend.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.UserEntity;

import java.time.LocalDateTime;

@Getter
@Accessors(fluent = true)
@RequiredArgsConstructor
public class SimpleGallery {
    private final Long id;
    private final String name;
    private final String description;
    private final String thumbnail;

    @JsonIgnoreProperties("roles")
    private final UserEntity creator;
    private final boolean privateGallery;
    private final LocalDateTime createdAt;
}
