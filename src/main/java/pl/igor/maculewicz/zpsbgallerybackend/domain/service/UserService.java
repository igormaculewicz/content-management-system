package pl.igor.maculewicz.zpsbgallerybackend.domain.service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.SetUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.RoleEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.UserEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.repository.UserRepository;
import pl.igor.maculewicz.zpsbgallerybackend.domain.dictionary.Role;
import pl.igor.maculewicz.zpsbgallerybackend.domain.exception.AlreadyExistsException;
import pl.igor.maculewicz.zpsbgallerybackend.domain.exception.NotFoundException;
import pl.igor.maculewicz.zpsbgallerybackend.utils.helper.RoleHelper;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Primary
@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;
    private final RoleService roleService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Cannot find user!"));

        Set<String> roles = user.roles().stream()
                .map(RoleEntity::name)
                .map(Role::name)
                .collect(Collectors.toSet());

        return new User(user.username(), user.passwordHash(), RoleHelper.grantRoles(roles));
    }

    public boolean exists(String username) {
        return userRepository.existsByUsername(username);
    }

    public UserEntity get(long id) throws NotFoundException {
        return userRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Cannot find user with id: %d", id)));
    }

    public UserEntity get(@NonNull String username) throws NotFoundException {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new NotFoundException(String.format("Cannot find user with name: %s", username)));
    }

    public UserEntity create(String username, String password, Role... roles) throws AlreadyExistsException {

        if (exists(username)) {
            throw new AlreadyExistsException("User already exists!");
        }

        Set<RoleEntity> roleEntities = roleService.getRoles(SetUtils.hashSet(roles));

        UserEntity user = new UserEntity()
                .username(username)
                .passwordHash(password);

        user.roles().addAll(roleEntities);

        return userRepository.save(user);
    }

    public UserEntity update(String username, String password, Role... roles) throws NotFoundException {

        Set<RoleEntity> roleEntities = roleService.getRoles(SetUtils.hashSet(roles));

        UserEntity user = userRepository.findByUsername(username)
                .orElseThrow(() -> new NotFoundException(String.format("Cannot find user: %s", username)));

        if (Strings.isNotBlank(username)) {
            user.username(username);
        }

        if (Strings.isNotBlank(password)) {
            user.passwordHash(password);
        }

        if (roles.length > 0) {
            Set<RoleEntity> existingRoles = user.roles();
            existingRoles.clear();
            existingRoles.addAll(roleEntities);
        }

        return userRepository.save(user);
    }

    public void delete(String username) throws NotFoundException {
        userRepository.delete(get(username));
    }
}
