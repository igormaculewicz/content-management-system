package pl.igor.maculewicz.zpsbgallerybackend.domain.converter.serializer;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.databind.util.Converter;

public class ContentToDescriptionConverter implements Converter<String, String> {

    private final static int DEFAULT_DESCRIPTION_SIZE = 250;

    @Override
    public String convert(String value) {
        int size = Math.min(value.length(), DEFAULT_DESCRIPTION_SIZE);

        return value.substring(0, size);
    }

    @Override
    public JavaType getInputType(TypeFactory typeFactory) {
        return typeFactory.constructType(String.class);
    }

    @Override
    public JavaType getOutputType(TypeFactory typeFactory) {
        return typeFactory.constructType(String.class);
    }
}
