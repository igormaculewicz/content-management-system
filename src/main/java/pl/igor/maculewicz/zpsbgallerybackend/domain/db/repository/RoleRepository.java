package pl.igor.maculewicz.zpsbgallerybackend.domain.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.RoleEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.dictionary.Role;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<RoleEntity, Long> {


    Optional<RoleEntity> findFirstByName(Role role);
}
