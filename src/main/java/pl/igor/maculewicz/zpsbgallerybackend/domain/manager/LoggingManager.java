package pl.igor.maculewicz.zpsbgallerybackend.domain.manager;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Slf4j
@Component
@RequiredArgsConstructor
public class LoggingManager {

    public void info(String format, Object... arguments) {
        String userMessage = String.format("[user: %s] ", getLoggedUsername());
        log.info(userMessage + format, arguments);
    }

    private Object getLoggedUsername() {
        return Optional.ofNullable(SecurityContextHolder.getContext())
                .map(SecurityContext::getAuthentication)
                .map(Authentication::getPrincipal)
                .orElse(UserManager.ANONYMOUS_USERNAME);
    }
}
