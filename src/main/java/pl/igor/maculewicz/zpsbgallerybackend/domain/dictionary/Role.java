package pl.igor.maculewicz.zpsbgallerybackend.domain.dictionary;

public enum Role {
    ROLE_USER, ROLE_ADMIN;
}
