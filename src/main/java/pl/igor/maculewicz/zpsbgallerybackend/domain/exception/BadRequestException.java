package pl.igor.maculewicz.zpsbgallerybackend.domain.exception;

public class BadRequestException extends Exception {

    public BadRequestException(String message) {
        super(message);
    }
}
