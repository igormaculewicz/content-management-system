package pl.igor.maculewicz.zpsbgallerybackend.domain.db.generator.generators;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import pl.igor.maculewicz.zpsbgallerybackend.config.AdminUserGeneratorConfiguration;
import pl.igor.maculewicz.zpsbgallerybackend.config.SampleDataConfiguration;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.NewsEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.UserEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.generator.DataGenerator;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.repository.NewsRepository;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.repository.UserRepository;
import pl.igor.maculewicz.zpsbgallerybackend.domain.exception.NotFoundException;
import pl.igor.maculewicz.zpsbgallerybackend.domain.service.NewsService;
import pl.igor.maculewicz.zpsbgallerybackend.domain.service.UserService;

import java.util.ArrayList;
import java.util.List;

@Component
@DependsOn("adminUserGenerator")
@RequiredArgsConstructor
public class NewsGenerator implements DataGenerator {

    private final UserService userService;
    private final NewsService newsService;
    private final AdminUserGeneratorConfiguration adminUserGeneratorConfiguration;
    private final SampleDataConfiguration sampleDataConfiguration;

    private final static String EXAMPLE_CONTENT = "\n" +
            "\n" +
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce varius efficitur venenatis. Curabitur sollicitudin purus euismod augue ornare, eget laoreet mi commodo. Nullam condimentum quam ac nunc porta, id venenatis orci facilisis. Suspendisse potenti. Etiam tristique nunc id elit pretium interdum. Suspendisse volutpat massa nisi, at tincidunt massa venenatis eget. Maecenas quam est, eleifend ac ante eget, aliquam commodo neque. Sed vitae ex a dolor pellentesque commodo eu vitae risus. Pellentesque nibh magna, lacinia eget libero a, feugiat hendrerit nisi. Donec tempor eros ac ex ultrices ultricies. Curabitur odio justo, convallis non consectetur a, volutpat et neque.\n" +
            "\n" +
            "Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras ac maximus mauris, quis scelerisque leo. Vivamus at fermentum enim. Cras accumsan, nunc a lacinia semper, sem tellus tincidunt augue, et vehicula quam ante eget ligula. In dignissim rhoncus urna sit amet sagittis. Aliquam et rhoncus nunc. Donec posuere sapien in mi porta, vitae tempus ante consectetur. Vivamus aliquam consequat felis, id dapibus nibh elementum tincidunt. Duis sit amet arcu ornare, aliquet lorem ut, dapibus dui. Curabitur vel accumsan lectus. In placerat, augue laoreet scelerisque semper, lacus justo hendrerit magna, vitae ornare nunc nisi vel arcu. Quisque congue libero a ornare congue. Donec odio ipsum, tincidunt eu eros rhoncus, mattis condimentum risus. Aenean vestibulum, odio ut consectetur convallis, ante magna volutpat erat, non consequat risus ex aliquam tellus. Mauris dignissim lacus eu condimentum accumsan. Fusce vehicula, augue ac suscipit ultricies, erat sapien dignissim elit, vitae ornare lectus mauris et augue.\n" +
            "\n" +
            "Proin mattis in turpis eget dictum. Donec laoreet arcu erat, sed congue nunc ultrices in. Quisque ac ligula metus. Vestibulum ante lectus, semper at metus et, sollicitudin dictum eros. Sed egestas porttitor dolor non dictum. Nulla condimentum, augue ut placerat sollicitudin, augue dui malesuada tortor, et porttitor nulla velit id risus. Fusce a diam eget massa convallis iaculis eget ut dolor. Sed sed porta massa.\n" +
            "\n" +
            "Cras malesuada elementum risus quis vehicula. Praesent pharetra, sapien at suscipit rhoncus, massa purus suscipit erat, vitae iaculis lacus elit vitae ligula. Etiam vitae interdum massa. Donec et eros ut magna viverra porta vitae at quam. Praesent vitae ligula sollicitudin, venenatis nibh ut, pulvinar arcu. Aliquam in tellus at ante commodo dictum. Nulla faucibus, magna ac ullamcorper ultricies, nisl orci placerat justo, condimentum fermentum libero leo eget nunc. Maecenas pellentesque sagittis nisl, at fermentum sem lacinia et. Nullam in lacus sed dolor consectetur volutpat sit amet at odio. Integer lobortis id eros ac pellentesque. Sed tincidunt nulla non pretium luctus. In diam nulla, elementum a turpis sit amet, imperdiet volutpat ante. Nunc ullamcorper tortor eget odio efficitur lobortis. Phasellus pharetra, nibh nec rhoncus bibendum, risus mi congue orci, non egestas sem dui quis orci. Aliquam scelerisque felis sed ullamcorper convallis. Nullam sit amet odio rhoncus, tincidunt lectus tempus, mollis eros.\n" +
            "\n" +
            "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Vestibulum pellentesque sem nunc, nec fringilla mauris fringilla et. Morbi at tortor eros. Fusce ullamcorper eget libero non vulputate. Vivamus consequat facilisis erat et ultrices. Vivamus vehicula, purus vitae rhoncus interdum, mauris augue blandit ante, vitae dignissim dolor sapien at nisl. In id turpis sit amet elit placerat semper. Quisque porttitor tristique suscipit. Vivamus convallis venenatis orci quis sodales.\n" +
            "\n" +
            "Integer lobortis vel urna id lobortis. Ut in pulvinar nisl. Nam in nibh sed ipsum dignissim ullamcorper sagittis id neque. Ut venenatis erat justo, vel posuere leo volutpat a. Nam tempor tortor a dictum tempus. Praesent lorem justo, porttitor et metus non, pretium auctor eros. Phasellus sed erat id felis vulputate tortor. " +
            "";

    @Override
    public void generate() throws NotFoundException {

        if (!sampleDataConfiguration.isNews()) {
            return;
        }

        UserEntity user = userService.get(adminUserGeneratorConfiguration.getUsername());

        for (int i = 0; i < 100; i++) {
            newsService.create("title " + i, EXAMPLE_CONTENT, i % 2 == 0, user);
        }
    }
}
