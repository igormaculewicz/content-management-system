package pl.igor.maculewicz.zpsbgallerybackend.domain.db.converter;

import pl.igor.maculewicz.zpsbgallerybackend.utils.crypto.CryptProvider;

import javax.persistence.AttributeConverter;

public class PasswordHashConverter implements AttributeConverter<String, String> {
    @Override
    public String convertToDatabaseColumn(String attribute) {
        return CryptProvider.getProvider().encode(attribute);
    }

    @Override
    public String convertToEntityAttribute(String dbData) {
        return dbData;
    }
}
