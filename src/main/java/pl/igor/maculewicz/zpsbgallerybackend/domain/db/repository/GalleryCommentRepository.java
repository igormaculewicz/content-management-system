package pl.igor.maculewicz.zpsbgallerybackend.domain.db.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.GalleryCommentEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.GalleryEntity;

@Repository
public interface GalleryCommentRepository extends JpaRepository<GalleryCommentEntity, Long> {

    Page<GalleryCommentEntity> getAllByGallery(GalleryEntity gallery, Pageable pageable);
}
