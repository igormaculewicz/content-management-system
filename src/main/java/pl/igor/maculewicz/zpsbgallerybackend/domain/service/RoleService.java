package pl.igor.maculewicz.zpsbgallerybackend.domain.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.RoleEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.repository.RoleRepository;
import pl.igor.maculewicz.zpsbgallerybackend.domain.dictionary.Role;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RoleService {

    private final RoleRepository roleRepository;


    public Set<RoleEntity> getRoles(Set<Role> roles) {

        return roles.stream()
                .map(role -> roleRepository.findFirstByName(role).orElse(new RoleEntity().name(role)))
                .map(roleEntity -> Objects.isNull(roleEntity.id()) ? roleRepository.save(roleEntity) : roleEntity)
                .collect(Collectors.toSet());
    }
}
