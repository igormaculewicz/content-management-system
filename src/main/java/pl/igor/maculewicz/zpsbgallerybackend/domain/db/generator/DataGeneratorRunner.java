package pl.igor.maculewicz.zpsbgallerybackend.domain.db.generator;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Set;

@Component
@RequiredArgsConstructor
public class DataGeneratorRunner {

    private final Set<DataGenerator> dataGenerators;

    @PostConstruct
    public void run() throws Exception {
        for (DataGenerator dataGenerator : dataGenerators) {
            dataGenerator.generate();
        }
    }
}
