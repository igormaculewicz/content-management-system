package pl.igor.maculewicz.zpsbgallerybackend.domain.exception;

public class UnauthorisedException extends Exception {

    public UnauthorisedException(String message) {
        super(message);
    }
}
