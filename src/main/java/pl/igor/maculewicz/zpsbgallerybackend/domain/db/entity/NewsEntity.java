package pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CreationTimestamp;
import pl.igor.maculewicz.zpsbgallerybackend.domain.model.SimpleNews;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@Entity(name = "news")
@Accessors(fluent = true)
public class NewsEntity {

    @Id
    @SequenceGenerator(name = "newsSeqGen", sequenceName = "news_id_seq", initialValue = 5, allocationSize = 100)
    @GeneratedValue(generator = "newsSeqGen")
    private Long id;

    @Column(length = 256)
    private String title;

    @Column(length = 4096, nullable = false)
    private String content;

    @Column(nullable = false)
    private boolean visible;

    @ManyToOne
    private UserEntity creator;

    @CreationTimestamp
    @Column(updatable = false, nullable = false)
    private LocalDateTime createdAt;

    @JsonValue
    public SimpleNews toSimpleNews() {
        return new SimpleNews(id, title, content, visible, creator, createdAt);
    }
}
























