package pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CreationTimestamp;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.converter.PasswordHashConverter;
import pl.igor.maculewicz.zpsbgallerybackend.domain.model.SimpleUser;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@Entity(name = "account")
@Accessors(fluent = true)
@ToString(exclude = {"galleries", "news"})
@EqualsAndHashCode(exclude = {"galleries", "news"})
public class UserEntity {

    @Id
    @SequenceGenerator(name = "userSeqGen", sequenceName = "user_id_seq", initialValue = 5, allocationSize = 100)
    @GeneratedValue(generator = "userSeqGen")
    private Long id;

    @Column(unique = true, nullable = false, length = 64)
    private String username;

    @JsonIgnore
    @Convert(converter = PasswordHashConverter.class)
    @Column(nullable = false, length = 64)
    private String passwordHash;

    @ManyToMany(cascade = CascadeType.MERGE)
    private Set<RoleEntity> roles = new HashSet<>();

    @OneToMany(mappedBy = "creator", cascade = CascadeType.ALL)
    private Set<GalleryEntity> galleries = new HashSet<>();

    @OneToMany(mappedBy = "creator", cascade = CascadeType.ALL)
    private Set<NewsEntity> news = new HashSet<>();

    @CreationTimestamp
    @Column(updatable = false, nullable = false)
    private LocalDateTime createdAt;

    @JsonValue
    public SimpleUser getSimpleUser() {
        return new SimpleUser(id, username, roles.stream().map(RoleEntity::name).collect(Collectors.toSet()), createdAt);
    }

}
























