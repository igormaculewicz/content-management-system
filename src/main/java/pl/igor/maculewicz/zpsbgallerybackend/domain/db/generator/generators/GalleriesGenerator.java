package pl.igor.maculewicz.zpsbgallerybackend.domain.db.generator.generators;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import pl.igor.maculewicz.zpsbgallerybackend.config.AdminUserGeneratorConfiguration;
import pl.igor.maculewicz.zpsbgallerybackend.config.SampleDataConfiguration;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.GalleryEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.UserEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.generator.DataGenerator;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.repository.GalleryRepository;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.repository.UserRepository;
import pl.igor.maculewicz.zpsbgallerybackend.domain.exception.NotFoundException;
import pl.igor.maculewicz.zpsbgallerybackend.domain.service.*;

import java.io.IOException;
import java.util.UUID;

@Component
@DependsOn("adminUserGenerator")
@RequiredArgsConstructor
public class GalleriesGenerator implements DataGenerator {

    private static final String EXAMPLE_FILENAME = "sample_image.jpeg";

    private final UserService userService;
    private final GalleryService galleryService;
    private final GalleryCommentService galleryCommentService;
    private final GalleryItemService galleryItemService;
    private final AdminUserGeneratorConfiguration adminUserGeneratorConfiguration;
    private final FileManagementService fileManagementService;
    private final SampleDataConfiguration sampleDataConfiguration;

    @Override
    public void generate() throws IOException, NotFoundException {

        if (!sampleDataConfiguration.isGalleries()) {
            return;
        }

        UserEntity user = userService.get(adminUserGeneratorConfiguration.getUsername());

        byte[] fileBytes = new ClassPathResource(EXAMPLE_FILENAME).getInputStream().readAllBytes();

        for (int i = 0; i < 30; i++) {

            GalleryEntity galleryEntity = galleryService.create("Gallery " + i, "Description of gallery " + 1, i % 2 == 0, user);


            for (int j = 0; j < 50; j++) {

                String filename = fileManagementService.saveFileWithRandomName(fileBytes).getFileName().toString();

                galleryItemService.create(galleryEntity, "Image " + j, "Description of image" + j, filename);
                galleryCommentService.create(galleryEntity, "Title " + j, "This is a comment " + j, user);
            }
        }
    }
}
