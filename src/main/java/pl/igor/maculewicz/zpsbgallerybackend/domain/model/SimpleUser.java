package pl.igor.maculewicz.zpsbgallerybackend.domain.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import pl.igor.maculewicz.zpsbgallerybackend.domain.dictionary.Role;

import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Accessors(fluent = true)
@RequiredArgsConstructor
public class SimpleUser {
    private final long id;
    private final String username;
    private final Set<Role> roles;
    private final LocalDateTime createdAt;
}
