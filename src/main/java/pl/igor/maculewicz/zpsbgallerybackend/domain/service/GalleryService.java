package pl.igor.maculewicz.zpsbgallerybackend.domain.service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.GalleryEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.UserEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.repository.GalleryRepository;
import pl.igor.maculewicz.zpsbgallerybackend.domain.exception.NotFoundException;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public class GalleryService {

    private final UserService userService;
    private final GalleryRepository galleryRepository;

    public GalleryEntity getById(long galleryId) throws NotFoundException {
        return galleryRepository.findById(galleryId)
                .orElseThrow(() -> new NotFoundException(String.format("Cannot find gallery with id: %d", galleryId)));
    }

    public Page<GalleryEntity> getAllPublic(@NonNull Pageable pageable) {
        return galleryRepository.getAllByPrivateGalleryIsFalse(pageable);
    }

    public Page<GalleryEntity> getPublicByUsername(@NonNull String username, @NonNull Pageable pageable) {
        return galleryRepository.getAllByCreatorUsernameAndPrivateGalleryIsFalse(username, pageable);
    }

    public Page<GalleryEntity> getAllByUsername(@NonNull String username, @NonNull Pageable pageable) {
        return galleryRepository.getAllByCreatorUsername(username, pageable);
    }

    public GalleryEntity create(String name, String description, boolean privateGallery, @NonNull UserEntity user) {

        GalleryEntity entity = new GalleryEntity()
                .name(name)
                .description(description)
                .privateGallery(privateGallery)
                .creator(user);

        return galleryRepository.save(entity);
    }

    public GalleryEntity update(@NonNull GalleryEntity gallery, @Nullable String name, @Nullable String description, @Nullable Boolean privateGallery) {

        if (Objects.nonNull(name)) {
            gallery.name(name);
        }

        if (Objects.nonNull(description)) {
            gallery.description(description);
        }

        if (Objects.nonNull(privateGallery)) {
            gallery.privateGallery(privateGallery);
        }

        return galleryRepository.save(gallery);
    }

    public void delete(GalleryEntity gallery) {
        galleryRepository.delete(gallery);
    }
}
