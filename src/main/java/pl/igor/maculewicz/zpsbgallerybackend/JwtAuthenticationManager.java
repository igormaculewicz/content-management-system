package pl.igor.maculewicz.zpsbgallerybackend;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import pl.igor.maculewicz.zpsbgallerybackend.domain.exception.UnauthorisedException;
import pl.igor.maculewicz.zpsbgallerybackend.domain.service.UserService;
import pl.igor.maculewicz.zpsbgallerybackend.utils.crypto.CryptProvider;
import pl.igor.maculewicz.zpsbgallerybackend.utils.crypto.TokenService;

@Service
@RequiredArgsConstructor
public class JwtAuthenticationManager {

    private final UserService userService;
    private final TokenService tokenService;

    public String getToken(@NonNull String username, @NonNull String password) throws UnauthorisedException {
        UserDetails userDetails = userService.loadUserByUsername(username);

        if (!CryptProvider.getProvider().matches(password, userDetails.getPassword())) {
            throw new UnauthorisedException("Wrong username or password!");
        }

        return tokenService.get(username, userDetails.getAuthorities());
    }
}