package pl.igor.maculewicz.zpsbgallerybackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableConfigurationProperties
@ComponentScan(basePackages="pl.*")
public class ZpsbGalleryBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZpsbGalleryBackendApplication.class, args);
    }

}

