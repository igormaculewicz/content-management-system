package pl.igor.maculewicz.zpsbgallerybackend.rest.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.UtilityClass;

import javax.validation.constraints.NotBlank;

@UtilityClass
public class AddImageItem {

    @Getter
    @Accessors(fluent = true)
    @RequiredArgsConstructor
    public static class Request {

        private final String name;
        private final String description;

        @NotBlank
        private final String encodedFile;
    }
}
