package pl.igor.maculewicz.zpsbgallerybackend.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import pl.igor.maculewicz.zpsbgallerybackend.config.BasicConfiguration;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.NewsEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.exception.ForbiddenException;
import pl.igor.maculewicz.zpsbgallerybackend.domain.exception.NotFoundException;
import pl.igor.maculewicz.zpsbgallerybackend.domain.exception.UnauthorisedException;
import pl.igor.maculewicz.zpsbgallerybackend.domain.manager.NewsManager;
import pl.igor.maculewicz.zpsbgallerybackend.domain.model.SimpleNews;
import pl.igor.maculewicz.zpsbgallerybackend.rest.model.CreateNews;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping(
        path = "/" + NewsResource.BASE_ENDPOINT_NAME,
        produces = MediaType.APPLICATION_JSON_VALUE
)
@RequiredArgsConstructor
public class NewsResource {

    final static String BASE_ENDPOINT_NAME = "news";

    private final NewsManager newsManager;
    private final BasicConfiguration basicConfiguration;

    @GetMapping
    public Page<SimpleNews> getAll(Pageable pageable) {
        return newsManager.getVisibleByPageRequest(pageable);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    public NewsEntity getById(@PathVariable long id) throws NotFoundException, ForbiddenException, UnauthorisedException {
        return newsManager.getVisibleById(id);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping
    public ResponseEntity<Void> create(@Valid @RequestBody CreateNews.Request request) throws UnauthorisedException {
        long id = newsManager.create(request.title(), request.content(), request.visible());

        return ResponseEntity.created(buildLocationHeader(id)).build();
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping(path = "/{id}")
    public NewsEntity update(@PathVariable long id, @Valid @RequestBody CreateNews.Request request) throws NotFoundException, UnauthorisedException, ForbiddenException {
        return newsManager.update(id, request.title(), request.content(), request.visible());
    }

    @DeleteMapping(path = "/{id}")
    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    public void delete(@PathVariable long id) throws UnauthorisedException, NotFoundException, ForbiddenException {
        newsManager.delete(id);
    }

    private URI buildLocationHeader(long id) {
        return URI.create(basicConfiguration.getMainUrl() + "/" + BASE_ENDPOINT_NAME + "/" + id);
    }
}
