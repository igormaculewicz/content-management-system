package pl.igor.maculewicz.zpsbgallerybackend.rest.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.UtilityClass;

import javax.validation.constraints.NotBlank;

@UtilityClass
public class UserLogin {

    @Getter
    @Accessors(fluent = true)
    @RequiredArgsConstructor
    public static class Request {

        @NotBlank
        private final String username;

        @NotBlank
        private final String password;
    }

    @Getter
    @Accessors(fluent = true)
    @RequiredArgsConstructor
    public static class Response {
        private final String token;
    }
}
