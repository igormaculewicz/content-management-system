package pl.igor.maculewicz.zpsbgallerybackend.rest.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.UtilityClass;

@UtilityClass
public class AddImageComment {

    @Getter
    @Accessors(fluent = true)
    @RequiredArgsConstructor
    public class Request {

        private final String title;
        private final String comment;
    }
}
