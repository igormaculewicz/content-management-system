package pl.igor.maculewicz.zpsbgallerybackend.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import pl.igor.maculewicz.zpsbgallerybackend.config.BasicConfiguration;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.GalleryEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.NewsEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.UserEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.exception.*;
import pl.igor.maculewicz.zpsbgallerybackend.domain.manager.GalleryManager;
import pl.igor.maculewicz.zpsbgallerybackend.domain.manager.NewsManager;
import pl.igor.maculewicz.zpsbgallerybackend.domain.manager.UserManager;
import pl.igor.maculewicz.zpsbgallerybackend.domain.model.SimpleNews;
import pl.igor.maculewicz.zpsbgallerybackend.rest.model.ChangePassword;
import pl.igor.maculewicz.zpsbgallerybackend.rest.model.UserLogin;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping(
        path = "/" + UserResource.BASE_ENDPOINT_NAME,
        produces = MediaType.APPLICATION_JSON_VALUE
)
@RequiredArgsConstructor
public class UserResource {

    final static String BASE_ENDPOINT_NAME = "user";

    private final GalleryManager galleryManager;
    private final NewsManager newsManager;
    private final UserManager userManager;
    private final BasicConfiguration basicConfiguration;

    @GetMapping
    public UserEntity get() throws NotFoundException, UnauthorisedException {
        return userManager.getCurrentUser();
    }

    @GetMapping(path = "/{username}/info")
    public UserEntity get(@PathVariable String username) throws NotFoundException, UnauthorisedException, ForbiddenException {
        return userManager.get(username);
    }

    @GetMapping(path = "/gallery")
    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    public Page<GalleryEntity> getCurrentUserGalleries(Pageable pageable) throws UnauthorisedException, ForbiddenException {
        return galleryManager.getAllByUsername(UserManager.getLoggedUser().getName(), pageable);
    }

    @GetMapping(path = "/news")
    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    public Page<SimpleNews> getCurrentUserNews(Pageable pageable) throws UnauthorisedException {
        return newsManager.getAllByUsername(UserManager.getLoggedUser().getName(), pageable);
    }

    @GetMapping(path = "/{username}/gallery")
    public Page<GalleryEntity> getUserPublicGalleries(@PathVariable String username, Pageable pageable) throws NotFoundException {
        return galleryManager.getPublicByUsername(username, pageable);
    }

    @GetMapping(path = "/{username}/news")
    public Page<SimpleNews> getUserVisibleNews(@PathVariable String username, Pageable pageable) {
        return newsManager.getVisibleByUsername(username, pageable);
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    public ResponseEntity create(@Valid @RequestBody UserLogin.Request request) throws BadRequestException, AlreadyExistsException {

        UserEntity user = userManager.createUser(request.username(), request.password());

        return ResponseEntity.created(buildLocationHeader(user.username())).build();
    }

    @PutMapping(path = "/change-password")
    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    public UserEntity changePassword(@Valid @RequestBody ChangePassword.Request request) throws UnauthorisedException, NotFoundException, BadRequestException {
        return userManager.changePassword(request.password());
    }

    @DeleteMapping
    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    public void delete() throws UnauthorisedException, ForbiddenException, NotFoundException {
        userManager.delete(UserManager.getLoggedUser().getName());
    }

    private URI buildLocationHeader(Object id) {
        return URI.create(basicConfiguration.getMainUrl() + "/" + BASE_ENDPOINT_NAME + "/" + id);
    }
}
