package pl.igor.maculewicz.zpsbgallerybackend.rest.config;

import lombok.RequiredArgsConstructor;
import org.apache.catalina.filters.CorsFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.AnonymousAuthenticationFilter;
import pl.igor.maculewicz.zpsbgallerybackend.rest.config.entrypoint.UnauthorisedJwtEntryPoint;
import pl.igor.maculewicz.zpsbgallerybackend.rest.config.filter.JwtAuthenticationFilter;
import pl.igor.maculewicz.zpsbgallerybackend.utils.crypto.CryptProvider;
import pl.igor.maculewicz.zpsbgallerybackend.utils.crypto.TokenService;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsService;
    private final UnauthorisedJwtEntryPoint unauthorisedJwtEntryPoint;
    private final TokenService tokenService;

    @Override
    protected void configure(HttpSecurity security) throws Exception {
        security.httpBasic().disable()
                .cors().and().csrf().disable()
                .headers().cacheControl().disable()
                .and()
                .authorizeRequests()
                .antMatchers("/authorization/**").permitAll()
                .antMatchers(HttpMethod.GET, "/news/**").permitAll()
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .exceptionHandling().authenticationEntryPoint(unauthorisedJwtEntryPoint)
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        security.addFilterBefore(new JwtAuthenticationFilter(tokenService), AnonymousAuthenticationFilter.class);
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(CryptProvider.getProvider());
    }
}
