package pl.igor.maculewicz.zpsbgallerybackend.rest.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.UtilityClass;

@UtilityClass
public class CreateGallery {

    @Getter
    @Accessors(fluent = true)
    @RequiredArgsConstructor
    public static class Request {
        private final String name;
        private final String description;
        private final boolean privateGallery;
    }

}
