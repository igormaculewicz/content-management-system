package pl.igor.maculewicz.zpsbgallerybackend.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.igor.maculewicz.zpsbgallerybackend.JwtAuthenticationManager;
import pl.igor.maculewicz.zpsbgallerybackend.domain.exception.UnauthorisedException;
import pl.igor.maculewicz.zpsbgallerybackend.rest.model.UserLogin;

@Slf4j
@RestController
@RequestMapping(
        path = "/authorization",
        produces = MediaType.APPLICATION_JSON_VALUE
)
@RequiredArgsConstructor
public class AuthorizationResource {

    private final JwtAuthenticationManager manager;

    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<UserLogin.Response> authorize(@Validated @RequestBody UserLogin.Request user) throws UnauthorisedException {
        return ResponseEntity.ok(new UserLogin.Response(manager.getToken(user.username(), user.password())));
    }
}
