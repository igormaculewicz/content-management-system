package pl.igor.maculewicz.zpsbgallerybackend.rest;

import lombok.RequiredArgsConstructor;
import org.apache.tika.Tika;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import pl.igor.maculewicz.zpsbgallerybackend.config.BasicConfiguration;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.GalleryCommentEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.GalleryEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.db.entity.GalleryItemEntity;
import pl.igor.maculewicz.zpsbgallerybackend.domain.exception.BadRequestException;
import pl.igor.maculewicz.zpsbgallerybackend.domain.exception.ForbiddenException;
import pl.igor.maculewicz.zpsbgallerybackend.domain.exception.NotFoundException;
import pl.igor.maculewicz.zpsbgallerybackend.domain.exception.UnauthorisedException;
import pl.igor.maculewicz.zpsbgallerybackend.domain.manager.GalleryManager;
import pl.igor.maculewicz.zpsbgallerybackend.rest.model.AddImageComment;
import pl.igor.maculewicz.zpsbgallerybackend.rest.model.AddImageItem;
import pl.igor.maculewicz.zpsbgallerybackend.rest.model.CreateGallery;

import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URI;

@RestController
@RequestMapping(
        path = "/" + GalleryResource.BASE_ENDPOINT_NAME,
        produces = MediaType.APPLICATION_JSON_VALUE
)
@RequiredArgsConstructor
public class GalleryResource {

    final static String BASE_ENDPOINT_NAME = "gallery";

    private final GalleryManager galleryManager;
    private final BasicConfiguration basicConfiguration;

    @GetMapping(path = "/{id}")
    public GalleryEntity get(@PathVariable long id) throws UnauthorisedException, NotFoundException, ForbiddenException {
        return galleryManager.get(id);
    }

    @GetMapping
    public Page<GalleryEntity> getAll(Pageable pageable) {
        return galleryManager.getAllPublic(pageable);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    public ResponseEntity<Void> create(@Valid @RequestBody CreateGallery.Request request) throws UnauthorisedException, NotFoundException {

        GalleryEntity gallery = galleryManager.create(request.name(), request.description(), request.privateGallery());

        return ResponseEntity.created(buildLocationHeader(gallery.id())).build();
    }

    @PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    public GalleryEntity update(@PathVariable long id, @Valid @RequestBody CreateGallery.Request request) throws ForbiddenException, UnauthorisedException, NotFoundException {
        return galleryManager.update(id, request.name(), request.description(), request.privateGallery());
    }

    @DeleteMapping(path = "/{id}")
    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    public void delete(@PathVariable long id) throws UnauthorisedException, NotFoundException, ForbiddenException {
        galleryManager.delete(id);
    }

    @GetMapping(path = "/{galleryId}/image")
    public Page<GalleryItemEntity> getImages(@PathVariable long galleryId, Pageable pageable) throws NotFoundException, ForbiddenException, UnauthorisedException {
        return galleryManager.getItems(galleryId, pageable);
    }

    @GetMapping(path = "/image/{imageName}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<byte[]> getImage(@PathVariable String imageName) throws IOException, UnauthorisedException, NotFoundException, ForbiddenException {

        byte[] imageBytes = galleryManager.getItem(imageName);
        MediaType mediaType = MediaType.parseMediaType(new Tika().detect(new ByteArrayInputStream(imageBytes)));

        return ResponseEntity.ok().contentType(mediaType).body(imageBytes);
    }

    @GetMapping(path = "/image/{imageName}/thumbnail", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<byte[]> getImageThumbnail(@PathVariable String imageName) throws IOException, NotFoundException, UnauthorisedException, ForbiddenException {

        byte[] imageBytes = galleryManager.getScaledItem(imageName, 250);

        return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(imageBytes);
    }

    @PostMapping(path = "/{galleryId}/image", consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    public ResponseEntity<Void> addImage(@PathVariable long galleryId, @Valid @RequestBody AddImageItem.Request request) throws NotFoundException, UnauthorisedException, ForbiddenException, IOException, BadRequestException {
        GalleryItemEntity galleryItem = galleryManager.addItem(galleryId, request.name(), request.description(), request.encodedFile());

        return ResponseEntity.created(URI.create(basicConfiguration.getMainUrl() + "/" + BASE_ENDPOINT_NAME + "/image/" + galleryItem.filename())).build();
    }

    @PutMapping(path = "/image/{galleryItemId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    public GalleryItemEntity updateImage(@PathVariable long galleryItemId, @RequestBody AddImageItem.Request request) throws ForbiddenException, UnauthorisedException, NotFoundException {
        return galleryManager.updateItem(galleryItemId, request.name(), request.description());
    }

    @DeleteMapping(path = "/image/{galleryItemId}")
    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    public void deleteImage(@PathVariable long galleryItemId) throws NotFoundException, UnauthorisedException, ForbiddenException {
        galleryManager.deleteItem(galleryItemId);
    }

    @GetMapping(path = "/comment/{galleryCommentId}")
    public GalleryCommentEntity getComment(@PathVariable long galleryCommentId) throws NotFoundException, UnauthorisedException, ForbiddenException {
        return galleryManager.getComment(galleryCommentId);
    }

    @GetMapping(path = "/{galleryId}/comment")
    public Page<GalleryCommentEntity> getComments(@PathVariable long galleryId, Pageable pageable) throws NotFoundException {
        return galleryManager.getComments(galleryId, pageable);
    }

    @PostMapping(path = "/{galleryId}/comment", consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    public ResponseEntity<Void> addComment(@PathVariable long galleryId, @Valid @RequestBody AddImageComment.Request request) throws NotFoundException, UnauthorisedException {
        GalleryCommentEntity galleryComment = galleryManager.addComment(galleryId, request.title(), request.comment());
        return ResponseEntity.created(URI.create(basicConfiguration.getMainUrl() + "/" + BASE_ENDPOINT_NAME + "/comment/" + galleryComment.id())).build();
    }

    @DeleteMapping(path = "/comment/{commentId}")
    @PreAuthorize("hasAnyRole('USER','ADMIN')")
    public void deleteComment(@PathVariable long commentId) throws NotFoundException, UnauthorisedException, ForbiddenException {
        galleryManager.deleteComment(commentId);
    }

    private URI buildLocationHeader(long id) {
        return URI.create(basicConfiguration.getMainUrl() + "/" + BASE_ENDPOINT_NAME + "/" + id);
    }
}
