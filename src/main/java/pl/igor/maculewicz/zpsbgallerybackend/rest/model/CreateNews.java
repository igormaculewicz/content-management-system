package pl.igor.maculewicz.zpsbgallerybackend.rest.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.UtilityClass;

import javax.validation.constraints.NotBlank;

@UtilityClass
public class CreateNews {

    @Getter
    @Accessors(fluent = true)
    @RequiredArgsConstructor
    public static class Request {
        private final String title;

        @NotBlank
        private final String content;
        private final Boolean visible;
    }
}
