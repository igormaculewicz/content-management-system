package pl.igor.maculewicz.zpsbgallerybackend.rest.config.filter;

import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.SetUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;
import pl.igor.maculewicz.zpsbgallerybackend.domain.dictionary.Role;
import pl.igor.maculewicz.zpsbgallerybackend.domain.manager.UserManager;
import pl.igor.maculewicz.zpsbgallerybackend.utils.crypto.TokenService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

@RequiredArgsConstructor
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private final static String TOKEN_PREFIX = "Bearer";

    private final TokenService tokenService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String authorisationHeader = request.getHeader(HttpHeaders.AUTHORIZATION);

        if (Objects.nonNull(authorisationHeader) && authorisationHeader.startsWith(TOKEN_PREFIX)) {
            String token = authorisationHeader.replace(TOKEN_PREFIX, "").trim();

            try {
                UsernamePasswordAuthenticationToken authentication = tokenService.verify(token);

                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                SecurityContextHolder.getContext().setAuthentication(authentication);
            } catch (Exception e) {
                logger.error("Cannot authenticate!", e);
                createAnonymousSession(request);
            }
        } else {
            createAnonymousSession(request);
        }

        filterChain.doFilter(request, response);
    }

    private void createAnonymousSession(HttpServletRequest request) {
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                UserManager.ANONYMOUS_USERNAME,
                "",
                SetUtils.hashSet(new SimpleGrantedAuthority(Role.ROLE_USER.name()))
        );

        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}
