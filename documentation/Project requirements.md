Celem jest utworzenie strony Galerii zawierającej: 

    Podstronę z Galeriami 
    Podstronę pojedynczej galerii 
    Możliwość dodawania komentarzy do galerii 
    Możliwość dodawania i zmieniania galerii przez formularz 
    Moduł Aktualności 

Należy w istniejącym projekcie umieścić: 

    Stronicowanie galerii 
    Formularz dodawania komentarzy do pojedynczej galerii 
    Wyświetlanie i usuwanie komentarzy 
    Formularz dodawania galerii 
    Formularz edycji galerii 
    Moduł Aktualności

Moduł aktualności powinien zawierać:

    Wyświetlanie listy aktualności od najnowszych 
    Stronicowanie aktualności 
    Dodawanie pojedynczej aktualności 
    Modyfikacja i usuwanie pojedynczej aktualności  