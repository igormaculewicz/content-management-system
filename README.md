# ZPSB gallery backend

### Requirements
+ **Java Runtime Environment 11+**
+ **Popular DBMS** (Configuration is specially for PostgreSQL, but can be switched for example to H2 in-memory database to avoid all DBMS configuration).

### How to run
To run application, simply clone this repository, configure application to your needs and then run:  
```sh
./gradlew clean bootRun
```

### How to release a package
Coming soon... :)

### Endpoints documentation
Here is a postman collection with up to date endpoints. Simply import it by this url:
https://www.getpostman.com/collections/a16cab10f6bad8e7ce96

### TODO
+ Code documentation
+ "How to release a package" section in readme
+ Better responses on errors(404/500/403/401/400).
+ Nice to have: Unit and integration tests